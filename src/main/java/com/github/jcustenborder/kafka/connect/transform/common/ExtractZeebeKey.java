package com.github.jcustenborder.kafka.connect.transform.common;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.config.ConfigDef;
import org.apache.kafka.connect.connector.ConnectRecord;
import org.apache.kafka.connect.data.Schema;
import org.apache.kafka.connect.data.Struct;
import org.apache.kafka.connect.transforms.Transformation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

public class ExtractZeebeKey<R extends ConnectRecord<R>> implements Transformation<R> {
    private static final Logger log = LoggerFactory.getLogger(ExtractZeebeKey.class);

    private ObjectMapper mapper = new ObjectMapper();

    @Override
    public R apply(R r) {
        try {
            String valueAsString = r.value().toString();
            Map<String, Object> valueAsMap = mapper.readValue(valueAsString, Map.class);
            log.info("ValueAsMap " + valueAsMap);
            Map variablesAsMap = (Map) valueAsMap.get("variablesAsMap");
            Long paymentId = Long.parseLong(variablesAsMap.get("paymentId").toString());
            log.info("Payment ID " + paymentId);

            return r.newRecord(
                    r.topic(),
                    r.kafkaPartition(),
                    Schema.INT64_SCHEMA,
                    paymentId,
                    Schema.STRING_SCHEMA,
                    mapper.writeValueAsString(variablesAsMap),
                    r.timestamp()
            );
        } catch (JsonProcessingException e) {
            throw new RuntimeException("Failed to convert", e);
        }
    }

    @Override
    public ConfigDef config() {
        return new ConfigDef();
    }

    @Override
    public void configure(Map<String, ?> settings) {


    }

    @Override
    public void close() {

    }
}
